#include <stdio.h>
#include <stdlib.h>

#include "BitmapHandler.h"

int LoadBitmapFromFile(FILE *file, BITMAP *bitmap);
int DecompressBitmapContent(unsigned int contentLength, BYTE contentToCreate[contentLength], unsigned int compressedLength, BYTE compressedContent[compressedLength], long bitmapWidth);
int SaveBitmapToFile(FILE *file, BITMAP *bitmap);
int convertTo24Bit(BITMAP *bitmap);
RECTANGLE* findRectangle(BITMAP *bitmap);
int validateRectangle(BITMAP *bitmap, int position, int* lengthX, int* lengthY);
int colorRectangles(BITMAP *bitmap, RECTANGLE *rectangles);

int GetFileSize(FILE* file) {
    int size = 0;

    fseek(file, 0L, SEEK_END);
    size = ftell(file);
    fseek(file, 0L, SEEK_SET);

    return size;
}

int LoadBitmapFromFilePath(const char *filename, BITMAP *bitmap) {
    int returnVal = 0;

    FILE *file = NULL;

    if ((file = fopen(filename, "rb")) == NULL) {
        printf("%s konnte nicht gefunden werden \n", filename);
        returnVal = 1;
    } else {
        returnVal = LoadBitmapFromFile(file, bitmap);
        fclose(file);
    }

    return returnVal;
}

int LoadBitmapFromFile(FILE *file, BITMAP *bitmap) {
    unsigned int fileSize = 0;
    unsigned int imageSize = 0;
    unsigned int colorBytes = 0;
    BYTE * compressedContent;
    
    int bla = 0;

    fileSize = GetFileSize(file);

    if (fread(&bitmap->fileHeader, sizeof (BITMAPFILEHEADER), 1, file) != 1) {
        printf("Einlesen der Datei war nicht erfolgreich \n");
        return 1;
    }

    if (ftell(file) != sizeof (BITMAPFILEHEADER)) {
        printf("Einlesen der Datei war nicht erfolgreich \n");
        return 1;
    }

    if (bitmap->fileHeader.bfType != 0x4d42) {
        printf("Einlesen der Datei war nicht erfolgreich. Datei beginnt nicht mit \"BM\" \n");
        return 1;
    }

    if (bitmap->fileHeader.bfSize != fileSize) {
        printf("Einlesen der Datei war nicht erfolgreich. Dateigröße entsricht nicht der Fileheader Angabe. \n");
        return 1;
    }

    if (bitmap->fileHeader.bfReserved1 != 0 ||
            bitmap->fileHeader.bfReserved2 != 0) {
        printf("Einlesen der Datei war nicht erfolgreich. Reserved Bytes sind korrupt. \n");
        return 1;
    }

    colorBytes = bitmap->fileHeader.bfOffBits - sizeof (BITMAPFILEHEADER) - sizeof (BITMAPINFOHEADER);

    bitmap->colorsLength = colorBytes / sizeof (RGBQUAD);
    if (colorBytes % sizeof (RGBQUAD) != 0) {
        printf("Einlesen der Datei war nicht erfolgreich. Die Palette entspricht nicht den Spezifikationen. \n");
        return 1;
    }

    if (fread(&bitmap->infoHeader, sizeof (BITMAPINFOHEADER), 1, file) != 1) {
        printf("Einlesen der Datei war nicht erfolgreich. Einlesen des Info Headers fehlgeschlagen. \n");
        return 1;
    }

    if (ftell(file) != sizeof (BITMAPFILEHEADER) + sizeof (BITMAPINFOHEADER)) {
        printf("Einlesen der Datei war nicht erfolgreich. Einlesen des Info Headers fehlgeschlagen. \n");
        return 1;
    }

    imageSize = bitmap->infoHeader.biWidth * bitmap->infoHeader.biHeight;

    if (bitmap->infoHeader.biSize != sizeof (BITMAPINFOHEADER)) {
        printf("Einlesen der Datei war nicht erfolgreich. Einlesen des Info Headers fehlgeschlagen. \n");
        return 1;
    }

    if (bitmap->infoHeader.biPlanes != 1) {
        printf("Einlesen der Datei war nicht erfolgreich. Fehler im Info Headers der Datei: Planes != 1 \n");
        return 1;
    }

    if (bitmap->infoHeader.biWidth < 1) {
        printf("Einlesen der Datei war nicht erfolgreich. Breite kleiner 0. \n");
        return 1;
    }

    if (bitmap->infoHeader.biHeight < 1) {
        printf("Einlesen der Datei war nicht erfolgreich. Höhe kleiner 0. \n");
        return 1;
    }

    if (bitmap->infoHeader.biWidth > 2048) {
        printf("Einlesen der Datei war nicht erfolgreich. Breite größer 2048. \n");
        return 1;
    }

    if (bitmap->infoHeader.biHeight > 2048) {
        printf("Einlesen der Datei war nicht erfolgreich. Höhe größer 2048. \n");
        return 1;
    }

    if (bitmap->infoHeader.biWidth % 4 != 0) {
        printf("Einlesen der Datei war nicht erfolgreich. Breite muss ein multiples von 4 sein. \n");
        return 1;
    }

    if (bitmap->infoHeader.biHeight % 4 != 0) {
        printf("Einlesen der Datei war nicht erfolgreich. Höhe muss ein multiples von 4 sein. \n");
        return 1;
    }

    if (bitmap->infoHeader.biBitCount != 8 &&
            bitmap->infoHeader.biBitCount != 24) {
        printf("Einlesen der Datei war nicht erfolgreich. Fehler im Info Header: Formatfehler \n");
        return 1;
    }

    if (bitmap->infoHeader.biCompression != 0 &&
            bitmap->infoHeader.biCompression != 1) {
        printf("Einlesen der Datei war nicht erfolgreich. Fehler im Info Header: Falsche Angabe bei Kompression. \n");
        return 1;
    }

    if (bitmap->infoHeader.biCompression == 0 &&
            bitmap->infoHeader.biSizeImage != imageSize) {
        printf("Einlesen der Datei war nicht erfolgreich. Fehler im Info Header: Image Size im InfoHeader stimmt nicht mit der realen Größe in der Datei überein \n");
        return 1;
    }

    if (fileSize - bitmap->fileHeader.bfOffBits != bitmap->infoHeader.biSizeImage) {
        printf("Einlesen der Datei war nicht erfolgreich. Fehler im Info Header: Falsche Angabe bei den Offbits. \n");
        return 1;
    }

    if ((bitmap->colors = malloc(colorBytes)) == NULL) {
        printf("Einlesen der Datei war nicht erfolgreich. Fehler beim Einlesen der Palette. \n");
        return 1;
    }

    if (fread(bitmap->colors, sizeof (RGBQUAD), bitmap->colorsLength, file) != bitmap->colorsLength) {
        free(bitmap->colors);
        bitmap->colors = NULL;
        printf("Einlesen der Datei war nicht erfolgreich. Fehler beim Einlesen der Palette. \n");
        return 1;
    }

    if (ftell(file) != bitmap->fileHeader.bfOffBits) {
        printf("Einlesen der Datei war nicht erfolgreich. Fehler beim Einlesen der Palette. \n");
        return 1;
    }

    if ((bitmap->content = malloc(imageSize)) == NULL) {
        free(bitmap->colors);
        bitmap->colors = NULL;
        printf("Einlesen der Datei war nicht erfolgreich. Fehler beim Einlesen der Pixeldaten. \n");
        return 1;
    }

    if (bitmap->infoHeader.biCompression == 0) {
        if (fread(bitmap->content, sizeof (BYTE), bitmap->infoHeader.biSizeImage, file) != bitmap->infoHeader.biSizeImage) {
            free(bitmap->colors);
            bitmap->colors = NULL;
            free(bitmap->content);
            bitmap->content = NULL;
            printf("Einlesen der Datei war nicht erfolgreich. Fehler beim Einlesen der Pixeldaten. \n");
            return 1;
        }
    } else if (bitmap->infoHeader.biCompression == 1 && bitmap->infoHeader.biBitCount == 8) {
        if ((compressedContent = malloc(sizeof (BYTE) * bitmap->infoHeader.biSizeImage)) == NULL) {
            free(bitmap->colors);
            bitmap->colors = NULL;
            free(bitmap->content);
            bitmap->content = NULL;
            printf("Einlesen der Datei war nicht erfolgreich. Fehler beim Einlesen der Pixeldaten. \n");
            return 1;
        }

        if (fread(compressedContent, sizeof (BYTE), bitmap->infoHeader.biSizeImage, file) != bitmap->infoHeader.biSizeImage) {
            free(bitmap->colors);
            bitmap->colors = NULL;
            free(bitmap->content);
            bitmap->content = NULL;
            free(compressedContent);
            compressedContent = NULL;
            printf("Einlesen der Datei war nicht erfolgreich. Fehler beim Einlesen der Pixeldaten. \n");
            return 1;
        }

        if (DecompressBitmapContent(imageSize, bitmap->content, bitmap->infoHeader.biSizeImage, compressedContent, bitmap->infoHeader.biWidth) != 0) {
            free(bitmap->colors);
            bitmap->colors = NULL;
            free(bitmap->content);
            bitmap->content = NULL;
            free(compressedContent);
            compressedContent = NULL;
            return 1;
        }

        bitmap->infoHeader.biSizeImage = imageSize;
        bitmap->infoHeader.biCompression = 0;

        free(compressedContent);
        compressedContent = NULL;
    } else {
        free(bitmap->colors);
        bitmap->colors = NULL;
        free(bitmap->content);
        bitmap->content = NULL;
        printf("Einlesen der Datei war nicht erfolgreich. Fehler beim Einlesen der Pixeldaten. \n");
        return 1;
    }

    return 0;
}

int DecompressBitmapContent(unsigned int contentLength, BYTE contentToCreate[contentLength], unsigned int compressedLength, BYTE compressedContent[compressedLength], long bitmapWidth) {
    int i, j = 0;
    int contentIndex = 0;
    BYTE currentByte;
    int bytesToInsert = 0;
    int deltaRight = 0;
    int deltaDown = 0;
    int absolutBytes = 0;

    for (i = 0; i < compressedLength; i++) {
        currentByte = compressedContent[i];

        if (currentByte == 0) {
            currentByte = compressedContent[++i];
            if (currentByte == 0) { //End of Line

                if (contentIndex % bitmapWidth == 0)
                    bytesToInsert = 0;
                else {
                    bytesToInsert = bitmapWidth - contentIndex % bitmapWidth;
                    currentByte = 0;
                }
            } else if (currentByte == 1) { // End of bitmap
                if (i == compressedLength - 1)
                    return 0;
                else {
                    printf("Einlesen der Datei war nicht erfolgreich. Fehler bei der Dekomprimierung: End of File Fehler \n");
                    return 1;
                }
            } else if (currentByte == 2) { // Delta
                deltaRight = compressedContent[++i];
                deltaDown = compressedContent[++i];

                bytesToInsert = (deltaDown * bitmapWidth) + deltaRight;
                currentByte = 0;
            } else { // Absolut Mode
                absolutBytes = currentByte;
                for (j = 0; j < absolutBytes; j++) {
                    currentByte = compressedContent[++i];
                    contentToCreate[contentIndex++] = currentByte;
                }
                if (absolutBytes % 2 == 1) {
                    i++;
                }
            }
        } else {
            bytesToInsert = currentByte;
            currentByte = compressedContent[++i];
        }

        while (bytesToInsert > 0) {
            if (contentIndex < contentLength) {
                contentToCreate[contentIndex++] = currentByte;
            } else {
                printf("Einlesen der Datei war nicht erfolgreich. Fehler bei der Dekomprimierung: Kein End of File gefunden. \n");
                return 1;
            }

            bytesToInsert--;
        }
    }


    printf("Einlesen der Datei war nicht erfolgreich. Fehler bei der Dekomprimierung: EOF Fehler \n");
    return 1;

}

void FreeHeapOfBitmap(BITMAP *bitmap) {
    if (bitmap->colors != NULL)
        free(bitmap->colors);
    bitmap->colors = NULL;

    if (bitmap->content != NULL)
        free(bitmap->content);
    bitmap->content = NULL;
}

int SaveBitmapToFilePath(const char *filename, BITMAP *bitmap) {
    int returnVal = 0;

    FILE *file = NULL;

    if ((file = fopen(filename, "w")) == NULL)
        returnVal = 1;
    else {
        returnVal = SaveBitmapToFile(file, bitmap);

        fclose(file);
    }

    return returnVal;
}

int SaveBitmapToFile(FILE *file, BITMAP *bitmap) {
    int i;
    RGBQUAD currentColor;

    bitmap->fileHeader.bfOffBits = sizeof (BITMAPFILEHEADER) + sizeof (BITMAPINFOHEADER);
    bitmap->infoHeader.biBitCount = 24;
    bitmap->infoHeader.biSizeImage = bitmap->infoHeader.biHeight * bitmap->infoHeader.biWidth;

    if (fwrite(&bitmap->fileHeader, sizeof (BITMAPFILEHEADER), 1, file) != 1) {
        printf("Fehler beim Speichern der Datei \n");
        return 1;
    }

    if (ftell(file) != sizeof (BITMAPFILEHEADER)) {
        printf("Fehler beim Speichern der Datei \n");
        return 1;
    }

    if (fwrite(&bitmap->infoHeader, sizeof (BITMAPINFOHEADER), 1, file) != 1) {
        printf("Fehler beim Speichern der Datei \n");
        return 1;
    }

    if (ftell(file) != sizeof (BITMAPFILEHEADER) + sizeof (BITMAPINFOHEADER)) {
        printf("Fehler beim Speichern der Datei \n");
        return 1;
    }

    //    if (fwrite(*bitmap->colors, sizeof (RGBQUAD), bitmap->colorsLength, file) != bitmap->colorsLength)
    //        return 1;

    if (ftell(file) != bitmap->fileHeader.bfOffBits) {
        printf("Fehler beim Speichern der Datei \n");
        return 1;
    }

    //    if (fwrite(bitmap->content, sizeof (BYTE), bitmap->infoHeader.biSizeImage, file) != bitmap->infoHeader.biSizeImage)
    //        return 1;

    for (i = 0; i < bitmap->infoHeader.biSizeImage; i++) {
        currentColor = bitmap->colors[bitmap->content[i]];

        if (fwrite(&currentColor, sizeof (RGBTRIPLE), 1, file) != 1) {
            printf("Fehler beim Speichern der Datei \n");
            return 1;
        }
    }

    return 0;
}

RECTANGLE* findRectangle(BITMAP *bitmap) {
    int lastColor = bitmap->content[0];
    int currentColor;
    int lengthX = 0;
    int lengthY = 0;
    RECTANGLE *rectangles = NULL;
    RECTANGLE * lastRectangle = NULL;
    int backgroundColorIndex = bitmap->content[0];

    int i;
    for (i = 0; i < (bitmap->infoHeader.biHeight * bitmap->infoHeader.biWidth); i++) {
        currentColor = bitmap->content[i];
        if (currentColor != backgroundColorIndex) {
            if (validateRectangle(bitmap, i, &lengthX, &lengthY) == 0) {

                if ((rectangles = (RECTANGLE*) malloc(sizeof (RECTANGLE))) == NULL) {
                    free(rectangles);
                    rectangles = NULL;
                }

                rectangles->upperLeftCorner = i;
                rectangles->lengthX = lengthX;
                rectangles->lengthY = lengthY;
                rectangles->nextRectangle = lastRectangle;
                lastRectangle = rectangles;
            }
            i += lengthX;
            lengthX = 0;
            lengthY = 0;
        }
        lastColor = currentColor;
    }
    return rectangles;
}

int validateRectangle(BITMAP *bitmap, int position, int* lengthX, int* lengthY) {

    int colorOfRectangle = bitmap->content[position];
    int currentColor = colorOfRectangle;
    int i = 0;
    int width = bitmap->infoHeader.biWidth;
    int height = bitmap->infoHeader.biHeight;

    while (currentColor == colorOfRectangle) {
        (*lengthX)++;
        i++;
        currentColor = bitmap->content[position + i];
    }
    i = 0;
    currentColor = colorOfRectangle;

    while (currentColor == colorOfRectangle) {
        (*lengthY)++;
        i++;
        currentColor = bitmap->content[position + (i * bitmap->infoHeader.biWidth)];
    }
    (*lengthX)--;
    (*lengthY)--;

    if ((*lengthX) < 4 || (*lengthY) < 4)
        return 1;

    if (bitmap->content[position + (*lengthX) + ((*lengthY) * width)] != colorOfRectangle)
        return 1;

    if (position > width) {
        for (i = position - width; i < position - width + (*lengthX); i++) {
            if (bitmap->content[i] == colorOfRectangle)
                return 1;
        }
    }
    if (position < (width * (height - 1))) {
        for (i = position + (width * ((*lengthY) + 1)); i < (position + (width * ((*lengthY) + 1)))+(*lengthX); i++) {
            if (bitmap->content[i] == colorOfRectangle)
                return 1;
        }
    }

    if (position % width > 1) {
        for (i = position - 1; i < (position - 1)+(width * (*lengthY)); i += width) {
            if (bitmap->content[i] == colorOfRectangle)
                return 1;
        }
    }

    if (position % width < width - 1) {
        for (i = position + (*lengthX) + 1; i < (position + (*lengthX) + 1) + width * (*lengthY); i += width) {
            if (bitmap->content[i] == colorOfRectangle)
                return 1;
        }
    }

    return 0;
}

int colorRectangles(BITMAP *bitmap, RECTANGLE *rectangles) {
    int upperleft, upperright, lowerleft, lowerright;
    int i, j;

    while (rectangles) {
        upperleft = rectangles->upperLeftCorner;
        upperright = upperleft + rectangles->lengthX;
        lowerleft = upperleft + (rectangles->lengthY * bitmap->infoHeader.biWidth);
        lowerright = lowerleft + rectangles->lengthX;

        for (j = upperleft; j < upperright; j++) {
            bitmap->content[j] = FRAMECOLOR;
        }

        for (j = lowerleft; j < lowerright; j++) {
            bitmap->content[j] = FRAMECOLOR;
        }

        for (j = upperleft; j < lowerleft + 1; j += bitmap->infoHeader.biWidth) {
            bitmap->content[j] = FRAMECOLOR;
        }

        for (j = upperright; j < lowerright + 1; j += bitmap->infoHeader.biWidth) {
            bitmap->content[j] = FRAMECOLOR;
        }
        rectangles = rectangles->nextRectangle;
    }
    return 0;
}
