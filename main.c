#include <stdio.h>
#include <stdlib.h>

#include "BitmapHandler.h"

int main(int argc, char** argv) {

    int returnVal = 0;

    //    BITMAP bitmap;
    //    RECTANGLE *rectangles = NULL;
    //
    //    returnVal = LoadBitmapFromFilePath("aufgabe4_bild3.bmp", &bitmap);
    //
    //    rectangles = findRectangle(&bitmap);
    //
    //    if (returnVal == 0)
    //        returnVal = colorRectangles(&bitmap, rectangles);
    //
    //    if (returnVal == 0)
    //        returnVal = SaveBitmapToFilePath("testSave.bmp", &bitmap);
    //
    //    FreeHeapOfBitmap(&bitmap);

    returnVal = recolorBitmapFile("untitled.bmp", "testOut.bmp");

    return returnVal;
}

int recolorBitmapFile(const char* readInFilename, const char* writeOutFilename) {
    int returnVal = 0;
    BITMAP bitmap;
    RECTANGLE *rectangles = NULL;

    printf("Load: %s\n", readInFilename);

    returnVal = LoadBitmapFromFilePath(readInFilename, &bitmap);

    if (returnVal == 0) {
        printf("Search for rectangles\n");
        rectangles = findRectangle(&bitmap);

        if (returnVal == 0) {
            printf("Insert rectangles in bitmap\n");
            returnVal = colorRectangles(&bitmap, rectangles);

            if (returnVal == 0) {
                printf("Save: %s\n", writeOutFilename);
                returnVal = SaveBitmapToFilePath(writeOutFilename, &bitmap);
            }
        }
        FreeHeapOfBitmap(&bitmap);
    }

    return returnVal;
}
