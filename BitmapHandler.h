typedef char CHAR;
typedef short SHORT;
//typedef long LONG;
typedef int LONG;
//long ist auf x64 64bit statt 32bit groß
//typedef unsigned long       DWORD;
typedef unsigned int DWORD;
typedef int BOOL;
typedef unsigned char BYTE;
typedef unsigned short WORD;

typedef struct tagBITMAPFILEHEADER {
    WORD bfType;
    DWORD bfSize;
    WORD bfReserved1;
    WORD bfReserved2;
    DWORD bfOffBits;
} __attribute__((__packed__)) BITMAPFILEHEADER, *PBITMAPFILEHEADER;

typedef struct tagBITMAPINFOHEADER {
    DWORD biSize;
    LONG biWidth;
    LONG biHeight;
    WORD biPlanes;
    WORD biBitCount;
    DWORD biCompression;
    DWORD biSizeImage;
    LONG biXPelsPerMeter;
    LONG biYPelsPerMeter;
    DWORD biClrUsed;
    DWORD biClrImportant;
} __attribute__((__packed__)) BITMAPINFOHEADER, *PBITMAPINFOHEADER;

typedef struct tagRGBQUAD {
    BYTE rgbBlue;
    BYTE rgbGreen;
    BYTE rgbRed;
    BYTE rgbReserved;
} __attribute__((__packed__)) RGBQUAD;

typedef struct tagBITMAPINFO {
    BITMAPINFOHEADER bmiHeader;
    RGBQUAD bmiColors[1];
} __attribute__((__packed__)) BITMAPINFO, *PBITMAPINFO;

typedef struct tagBITMAP {
    BITMAPFILEHEADER fileHeader;
    BITMAPINFOHEADER infoHeader;
    int colorsLength;
    RGBQUAD *colors;
    BYTE *content;

} BITMAP;

typedef struct tagRGBTRIPLE {
    BYTE rgbtBlue;
    BYTE rgbtGreen;
    BYTE rgbtRed;
} RGBTRIPLE;

typedef struct rectangle {
    int upperLeftCorner;
    int lengthX;
    int lengthY;
    struct rectangle *nextRectangle;
} RECTANGLE;

#define BACKGROUNDCOLOR 0xd7     // 0xd7 = weiß
#define FRAMECOLOR 0x28          // 0x28 = blau

int LoadBitmapFromFilePath(const char *filename, BITMAP *bitmap);

void FreeHeapOfBitmap(BITMAP *bitmap);

int SaveBitmapToFilePath(const char *filename, BITMAP *bitmap);

RECTANGLE* findRectangle(BITMAP *bitmap);

int colorRectangles(BITMAP *bitmap, RECTANGLE *rectangles);
